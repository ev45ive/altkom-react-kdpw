import React, { Suspense } from "react";
import "./App.css";
import MusicSearchView from "./views/MusicSearchView";
import Layout from "./components/Layout";
import MusicSearchContextView from "./views/MusicSearchContextView";
import MusicProvider from "./containers/MusicProvider";
import PlaylistsReduxView from "./views/PlaylistsReduxView";
import { Route, Switch, Redirect } from "react-router";

// https://pl.reactjs.org/docs/code-splitting.html#reactlazy
// https://reacttraining.com/react-router/web/guides/code-splitting
const MusicSearchViewLazy = React.lazy(() => import("./views/MusicSearchView"));

// React.createElement('div',{id:123},'Ala ma kota')
// <div id="123">Ala ma kota</div>

class App extends React.Component {
  render() {
    return (
      <Layout>
        <MusicProvider>
          <Switch>
            <Redirect path="/" to="/playlists" exact={true} />
            <Route path="/playlists/:id" component={PlaylistsReduxView} />
            <Route path="/playlists" component={PlaylistsReduxView} />
            <Route path="/search" render={()=><Suspense fallback="loading...">
              <MusicSearchViewLazy/>
            </Suspense>} />
            <Route path="*" render={() => <h1>404 Not Found</h1>} />
          </Switch>
        </MusicProvider>
      </Layout>
    );
  }
}

export default App;
