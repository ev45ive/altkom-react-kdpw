import { connect } from "react-redux";
import ItemsList from "../components/ItemsList";
import { Playlist } from "../models/Playlist";
import {
  playlistsSelect,
  selectedPlaylistSelector
} from "../reducers/playlists.reducer";
import { bindActionCreators } from "redux";
import { withRouter, useParams, RouteComponentProps } from "react-router";

type State = {
  playlists: {
    items: Playlist[];
    selectedId: Playlist["id"];
  };
};
type OwnProps = RouteComponentProps<{ id: string }>;

export const PlaylistsList = withRouter(
  connect(
    /* mapStateToProps */
    (state: State, ownProps: OwnProps) => {
        return ({
            playlists: state.playlists.items,
            selected: state.playlists.items.find(p => p.id == +ownProps.match.params.id)!
          })
    },

    /* mapDispatchToProps */
    dispatch =>
      bindActionCreators(
        {
        //   onSelect: playlistsSelect
          /* onSelect: (p:Playlist) =>  playlistSelectId(p.id), */
        },
        dispatch
      )
  )(ItemsList)
);

//   dispatch => ({
//     onSelect(selected: Playlist) {
//       dispatch(playlistsSelect(selected));
//     }
//   })
