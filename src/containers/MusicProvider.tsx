import * as React from "react";
import { musicContext, musicSearch } from "../services";
import { Album } from "../models/Album";

type S = {
  query: string;
  results: Album[];
};

export default class MusicProvider extends React.Component<{}, S> {
  state: S = {
    query: "batman",
    results: [
      {
        id: "123",
        name: "Test",
        images: [
          {
            url: "https://www.placecage.com/gif/200/300"
          }
        ]
      }
    ]
  };

  search = (query: string) => {
    musicSearch.searchAlbums(query).then(results =>
      this.setState({
        results
      })
    );
  };

  render() {
    return (
      <musicContext.Provider
        value={{
          query: this.state.query,
          results: this.state.results,
          search: this.search
        }}
      >
        {this.props.children}
      </musicContext.Provider>
    );
  }
}
