import { Playlist } from "../models/Playlist";
import { connect } from "react-redux";
import PlaylistDetails from "../components/PlaylistDetails";
import PlaylistForm from "../components/PlaylistForm";
import { bindActionCreators } from "redux";
import {
  selectedPlaylistSelector,
  playlistsUpdate
} from "../reducers/playlists.reducer";
import { RouteComponentProps } from "react-router";

type State = {
  playlists: {
    items: Playlist[];
    selectedId: Playlist["id"];
  };
};
type OwnProps = RouteComponentProps<{ id: string }>;

export const withSelectedPlaylist = connect(
  (state: State, ownProps: OwnProps) => ({
    playlist: state.playlists.items.find(p => p.id == +ownProps.match.params.id)!
  }),
  dispatch =>
    bindActionCreators(
      {
        //   onEdit(){ },
        //   onCancel(){},
        onSave: playlistsUpdate
      },
      dispatch
    )
);

export const SelectedPlaylistDetails = withSelectedPlaylist(PlaylistDetails);
export const SelectedPlaylistForm = withSelectedPlaylist(PlaylistForm);

// export const withSelectedPlaylist = connect(
//     (state:State)=>({

//     }),
//     dispatch => bindActionCreators({},dispatch)
// );
