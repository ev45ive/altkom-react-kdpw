import React /* , { Fragment } */ from "react";
import ItemsList from "../components/ItemsList";
import PlaylistDetails from "../components/PlaylistDetails";
import PlaylistForm from "../components/PlaylistForm";
import { Playlist } from "../models/Playlist";
import { store } from "../store";
import { Unsubscribe } from "redux";

type S = {
  query: string;
  mode: "show" | "edit";
  playlists: Playlist[];
  selected: Playlist | null;
};

class PlaylistsView extends React.Component<{}, S> {
  state: S = {
    query: "",
    mode: "show",
    playlists: [],
    selected: null
  };

  select = (selected: Playlist) => {
    this.setState(prevState => ({
      selected: selected === prevState.selected ? null : selected
    }));
  };

  edit = () => this.setState({ mode: "edit" });

  cancel = () => this.setState({ mode: "show" /* , selected:null  */ });

  save = (draft: Playlist) => {
    this.setState(prevState => {
      /* Create copy of playlists to indicate something inside changed: */
      const playlists = prevState.playlists.map(p =>
        p.id === draft.id ? draft : p
      );

      return { mode: "show", playlists, selected: draft };
    });
  };

  // constructor(props:{}){
  //   super(props);
  //   this.state.selected = this.state.playlists[0]
  // }

  render() {
    return (
      <div className="row">
        <div className="col">
          <input
            type="text"
            className="form-control"
            value={this.state.query}
            onChange={e => this.setState({ query: e.target.value })}
          />
          <ItemsList
            onSelect={this.select}
            playlists={this.state.playlists}
            selected={this.state.selected}
          />
        </div>

        <div className="col">
          {this.state.selected && this.state.mode === "show" && (
            <>
              <PlaylistDetails
                playlist={this.state.selected}
                onEdit={this.edit}
              />
            </>
          )}

          {this.state.selected && this.state.mode === "edit" && (
            <>
              <PlaylistForm
                playlist={this.state.selected}
                onCancel={this.cancel}
                onSave={this.save}
              />
            </>
          )}

          {!this.state.selected && <p>Please select playlist</p>}
        </div>
      </div>
    );
  }
}

export default PlaylistsView;
