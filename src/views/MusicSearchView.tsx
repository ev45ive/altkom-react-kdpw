import * as React from "react";
import SearchForm from "../components/SearchForm";
import SearchResults from "../components/SearchResults";
import { Album } from "../models/Album";
import { musicSearch } from "../services";
// rcc
// tsrcc

type S = {
  results: Album[];
};

export default class MusicSearchView extends React.Component<{}, S> {
  state: S = {
    results: [
      {
        id: "123",
        name: "Test",
        images: [
          {
            url: "https://www.placecage.com/gif/200/300"
          }
        ]
      }
    ]
  };

  componentDidMount() {
    this.search('batman');
  }
  
  search = (query:string)  => {
    musicSearch
      .searchAlbums(query)
      .then(results => this.setState({
        results
      }));
  }

  render() {
    return (
      <div>
        <div className="row">
          <div className="col">
            <SearchForm onSearch={this.search} />
          </div>
        </div>
        <div className="row">
          <div className="col">
            <SearchResults results={this.state.results} />
          </div>
        </div>
      </div>
    );
  }
}
