import * as React from "react";
import SearchForm from "../components/SearchForm";
import SearchResults from "../components/SearchResults";
import { musicContext } from "../services";

const MusicSearchContextView = () => {
  const context = React.useContext(musicContext);

  React.useEffect(() => {
    context.search("batman");

    // return () => cancelRequest()
  }, [true /* , context.query */]);

  return (
    <div>
      <div className="row">
        <div className="col">
          <musicContext.Consumer>
            {context => <SearchForm onSearch={context.search} />}
          </musicContext.Consumer>
        </div>
      </div>
      <div className="row">
        <div className="col">
          <musicContext.Consumer>
            {({ results, query }) => <SearchResults results={results} />}
          </musicContext.Consumer>
        </div>
      </div>
    </div>
  );
};

export default MusicSearchContextView;

// /* Higher Order Components */
// function withMusicCtx<P>(Comp: React.ComponentType<P>) {
//   return (props: P) => (
//     <musicContext.Consumer>
//         {(context) => <Comp {...props} {...context} />}
//     </musicContext.Consumer>
//   );
// }

// const componentWithMusic = withMusicCtx(ComponentWithoutMusic)