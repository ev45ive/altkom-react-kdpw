import React from "react";
import { PlaylistsList } from "../containers/PlaylistsList";
import {
  SelectedPlaylistDetails,
  SelectedPlaylistForm
} from "../containers/Playlist";
import { Route, Switch, RouteComponentProps } from "react-router";
// npm i --save react-redux @types/react-redux
// rcc

type P = RouteComponentProps;

class PlaylistsReduxView extends React.Component<P> {
  render() {
    return (
      /* .row>.col*2 */
      <div className="row">
        <div className="col">
          <PlaylistsList
            onSelect={playlist => {
              this.props.history.push({
                pathname: "/playlists/" + playlist.id
              });
            }}
          />
        </div>

        <div className="col">
          <Switch>
            <Route
              path="/playlists/:id/edit"
              component={SelectedPlaylistForm}
            />
            <Route
              path="/playlists/:id"
              component={(props: any) => (
                <SelectedPlaylistDetails
                  {...props}
                  onEdit={() =>
                    this.props.history.push({
                      pathname: "/playlists/" + props.match.params.id + "/edit"
                    })
                  }
                />
              )}
            />
            <Route
              path="/playlists/"
              render={() => <p>Please select playlist!</p>}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

export default PlaylistsReduxView;
