export interface Playlist{
    id: number
    name: string
    favorite:boolean
    /**
     * HEX color
     */
    color: string
    tracks?: Track[]
}

export interface Track{
    id: number 
    name: string
}

/* === */

// p.id = 123

// if(p.tracks){
//     p.tracks.length
// }

// if( typeof p.id == 'string'){
//     p.id.charCodeAt(1)
// }else{
//     p.id.toExponential()
// }

// interface Point{x:number; y:number}
// interface Vector{x:number; y:number, length:number}

// const v1:Vector = {x:1,y:2, length:12}
// const p1:Point = v1;