export interface Entity {
  id: string;
  name: string;
}

export interface Album extends Entity {
  images: AlbumImage[];
  artists?: Artist[];
}

export interface Artist extends Entity {
  popularity: number;
}

export interface AlbumImage {
  url: string;
}

export interface PagingObject<T> {
  items: T[];
  total?: number;
}

export interface AlbumsResponse {
  albums: PagingObject<Album>;
}
