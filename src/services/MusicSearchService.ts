import { AuthService } from "./AuthService";
import { AlbumsResponse, Album } from "../models/Album";
import axios, { AxiosResponse } from "axios"; //npm i --save axios @types/axios

export class MusicSearchService {
  constructor(private url: string, private auth: AuthService) {}

  searchAlbums(query = "batman") {
    return axios
      .get<AlbumsResponse>(this.url, {
        params: {
          type: "album",
          q: query
        },
        headers: {
          Authorization: "Bearer " + this.auth.getToken()
        }
      })
      .then(response => response.data.albums.items)
      .catch(err => {
        if (err.isAxiosError && err.response.status === 401) {
          this.auth.authorize();
        }
        return [] as Album[];
      });
  }

  // searchAlbums(query = "batman") {
  //   return fetch(`${this.url}?type=album&q=${query}`, {
  //     headers: {
  //       Authorization: "Bearer " + this.auth.getToken()
  //     }
  //   })
  //     .then(resp => {
  //       if (resp.status == 401) {
  //         this.auth.authorize();
  //       }
  //       if (resp.status !== 200) {
  //         return Promise.reject(resp);
  //       }
  //       return resp;
  //     })
  //     .then<AlbumsResponse>(resp => resp.json())
  //     .then(resp => resp.albums.items);
  // }
}
