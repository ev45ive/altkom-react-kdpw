import { createStore, combineReducers } from "redux"; //  npm i --save redux @types/redux
import { counter } from "./reducers/counter";
import { playlists } from "./reducers/playlists.reducer";

export const rootReducer = combineReducers({
  counter: counter,
  playlists: playlists
});

const initialState = {
  playlists: {
    items: [
      {
        id: 123,
        name: "React Hits!",
        favorite: true,
        color: "#ff00ff"
      },
      {
        id: 234,
        name: "React Top20",
        favorite: true,
        color: "#00ffff"
      },
      {
        id: 345,
        name: "Best of React!",
        favorite: true,
        color: "#ffff00"
      }
    ],
    selectedId: 123
  }
};

export const store = createStore(
  rootReducer,
  initialState,
  (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
    (window as any).__REDUX_DEVTOOLS_EXTENSION__()
);

(window as any).store = store;
