import React, { FC } from "react";
import AlbumCard from "./AlbumCard";
import { Album } from "../models/Album";

interface Props {
  results: Album[];
}

const SearchResults: FC<Props> = ({ results }) => {
  return (
    <div>
      <div className="card-group">
        {results.map(result => (
          <AlbumCard album={result} key={result.id} />
        ))}
      </div>
    </div>
  );
};

export default SearchResults;

// https://getbootstrap.com/docs/4.3/
//              components/card/#card-groups
