import React, { FC } from "react";
import { Playlist } from "../models/Playlist";

type P = { playlist: Playlist; onEdit(): void };

const PlaylistDetails: FC<P> = React.memo(({ playlist, onEdit }) => {
  return (
    <div>
      <dl data-id={playlist.id} title={playlist.name}>
        <dt>Name:</dt>
        <dd>{playlist.name}</dd>

        <dt>Favourite:</dt>
        <dd>{playlist.favorite ? "Yes" : "No"}</dd>

        <dt>Color:</dt>
        <dd style={{ color: playlist.color, background: playlist.color }}>
          {playlist.color}
        </dd>
      </dl>

      <input type="button" value="Edit" onClick={onEdit} />
    </div>
  );
});

export default PlaylistDetails;
