import React, { Component, PureComponent } from "react";
import { Playlist } from "../models/Playlist";

type S = {
  playlist: Playlist;
};

type P = {
  playlist: Playlist;
  onCancel(): void;
  onSave(draft: Playlist): void;
  // onSaveDraft: () => void;
};

export class PlaylistForm extends PureComponent<P, S> {
  state: S = {
    playlist: this.props.playlist
  };

  constructor(props: P) {
    super(props);
    console.log("constructor");
    // bad pracice! won`t refresh! use getDerivedStateFromProps()
    // this.state.playlist = this.props.playlist
  }

  static getDerivedStateFromProps(nextProps: P, nextState: S) {
    console.log("getDerivedStateFromProps");

    return {
      playlist:
        nextProps.playlist.id === nextState.playlist.id
          ? nextState.playlist
          : nextProps.playlist
    };
  }

  // shouldComponentUpdate(nextProps: P, nextState: S) {
  //   console.log("shouldComponentUpdate");
  //   return (
  //     this.props.playlist !== nextProps.playlist ||
  //     this.state.playlist !== nextState.playlist
  //   );
  // }

  handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const target = event.currentTarget;
    const fieldName = target.name;
    const value = target.type === "checkbox" ? target.checked : target.value;

    // (this.state.playlist as any)[fieldName] = value;

    this.setState(prevState => ({
      playlist: {
        ...prevState.playlist,
        [fieldName]: value
      }
    }));
  };

  componentWillUnmount() {
    console.log("componentWillUnmount");
  }

  componentDidMount() {  console.log("componentDidMount");

    if (this.nameInputRef.current) {
      this.nameInputRef.current.focus();
    }
  }

  getSnapshotBeforeUpdate() { console.log("getSnapshotBeforeUpdate");
    return {
      hasFocus: document.activeElement === this.nameInputRef.current
    };
  }

  componentDidUpdate(props: P, state: S, snapshot: { hasFocus: boolean }) {
    console.log("componentDidUpdate");
    snapshot.hasFocus && this.nameInputRef.current!.focus();
  }
  
  nameInputRef = React.createRef<HTMLInputElement>();

  render() {
    console.log("render");
    return (
      <div>
        {/* {JSON.stringify(this.state)} */}
        <div className="form-group">
          <label>Name:</label>
          <input
            ref={this.nameInputRef}
            type="text"
            onChange={this.handleInput}
            value={this.state.playlist.name}
            name="name"
            className="form-control"
          />
          {170 - this.state.playlist.name.length} / 170
        </div>

        <div className="form-group">
          <label>Favorite:</label>
          <input
            type="checkbox"
            checked={this.state.playlist.favorite}
            name="favorite"
            onChange={this.handleInput}
          />
        </div>

        <div className="form-group">
          <label>Color:</label>
          <input
            type="color"
            value={this.state.playlist.color}
            name="color"
            onChange={this.handleInput}
          />
        </div>

        <input type="button" value="Cancel" onClick={this.props.onCancel} />
        <input
          type="button"
          value="Save"
          onClick={() => this.props.onSave(this.state.playlist)}
        />
      </div>
    );
  }
}

export default PlaylistForm;
