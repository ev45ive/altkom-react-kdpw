import React, { Component } from "react";

type P = {
  onSearch(query:string):void
}
type S = {
  query:string
}

export default class SearchForm extends Component<P,S> {
  state:S = {query:'superman'}

  handleInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({query: event.currentTarget.value})
  }

  handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) =>{
    if(event.key === 'Enter'){
      this.props.onSearch(this.state.query)
    }
  }

  render() {
    return (
      <div>
        <div className="input-group mb-3">
          
          <input type="text" className="form-control" 
                onKeyDown={this.handleKeyDown}
                onChange={this.handleInput}
                placeholder="Search" />
          
          <div className="input-group-append">
            <button className="btn btn-outline-secondary"
                    onClick={() => this.props.onSearch(this.state.query)  }>
                Search
            </button>
          </div>

        </div>
      </div>
    );
  }
}
