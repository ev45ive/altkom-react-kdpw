// npm i -D @types/jest react-test-renderer @types/react-test-renderer
// https://www.pluralsight.com/guides/how-to-test-react-components-in-typescript
import renderer from "react-test-renderer";
// import ReactTestUtils from 'react-test-utils'
// import { render, fireEvent, waitForElement } from "@testing-library/react";

// https://sentry.io/for/react/
// https://docs.sentry.io/platforms/javascript/react/#error-boundaries

import React from "react";
import PlaylistDetails from "./PlaylistDetails";
import { Playlist } from "../models/Playlist";
import {
  Simulate,
  createRenderer,
  findRenderedDOMComponentWithTag
} from "react-dom/test-utils";

describe("PlaylistDetails", () => {
  let mock: Playlist;
  let component: renderer.ReactTestRenderer;
  let onEdit: jest.Mock<any, any>;
  let container: HTMLDivElement;

  beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });

  afterEach(() => {
    document.body.removeChild(container);
  });

  beforeEach(() => {
    mock = {
      id: 123,
      name: "Testowa playlista",
      favorite: true,
      color: "#ff00ff"
    };
    onEdit = jest.fn();
    component = renderer.create(
      <PlaylistDetails playlist={mock} onEdit={onEdit} />
    );
  });

  it("render playlist details", () => {
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should emit onEdit when button clicked", () => {
    const renderer = createRenderer();
    renderer.render(<PlaylistDetails playlist={mock} onEdit={onEdit} />);
    const compoenent = renderer.getRenderOutput();

    // const button = findRenderedDOMComponentWithTag(compoenent, "input");
    // const button = component.root.find(e => e.type == "input");
    // expect(button).toBeTruthy();
    // Simulate.click(button);
    expect(onEdit).toHaveBeenCalled();
  });
});
