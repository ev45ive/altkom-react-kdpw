import React, { FC } from "react";
import { Album } from "../models/Album";

type Props = { album: Album};

const AlbumCard: FC<Props> = ({album}) => {
  return (
    <div className="card">
      <img src={album.images[0].url} 
            className="card-img-top" />
      
      <div className="card-body">
        <h5 className="card-title">{album.name}</h5>
      </div>
    </div>
  );
};
export default AlbumCard;
