import React, { Component, PureComponent } from "react";
import { Playlist } from "../models/Playlist";

type P = {
  playlists: Playlist[];
  selected: Playlist | null;
  onSelect(selected: Playlist): void;
};

class ItemsList extends PureComponent<P> {

  select = (selected: Playlist) => this.props.onSelect(selected);

  render() {
    return (
      <div>
        <div className="list-group">
          {this.props.playlists.map((playlist, index) => {
            const selected = playlist === this.props.selected;

            return (
              <div
                className={`list-group-item ${selected ? "active" : ""}`}
                onClick={event => this.select(playlist)}
                key={playlist.id}
              >
                {index + 1}. {playlist.name}
              </div>
            );
          })}
        </div>
      </div>
    );
  }
}

export default ItemsList;
