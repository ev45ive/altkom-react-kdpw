import React, { FC } from "react";
import { NavLink, Link } from 'react-router-dom'

const Layout: FC = props => {
  console.log(props);

  return (
    <>
      <nav className="navbar navbar-expand navbar-dark bg-dark mb-3">
        <div className="container">
          <NavLink className="navbar-brand" to="/">
            MusicApp
          </NavLink>

          <div className="collapse navbar-collapse">
            <ul className="navbar-nav">
              <li className="nav-item">
                <NavLink className="nav-link" to="/search">
                  Search
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink className="nav-link" to="/playlists">
                  Playlists
                </NavLink>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div className="container">
        <div className="row">
          <div className="col">{props.children}</div>
        </div>
      </div>
    </>
  );
};

export default Layout;
