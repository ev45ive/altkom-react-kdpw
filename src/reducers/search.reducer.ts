import { Album } from "../models/Album"


const initialState = {
    query:'',
    results: [] as Album[]
}

export default (state = initialState, { type, payload }:any) => {
    switch (type) {

    case 'SEARCH_SUCCESS':
        return { ...state, ...payload }

    default:
        return state
    }
}
