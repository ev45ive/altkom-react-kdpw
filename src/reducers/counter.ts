import { Action, ActionCreator, Reducer } from "redux";

interface INC extends Action<"INC"> {
  payload: number;
}

interface DEC extends Action<"DEC"> {
  payload: number;
}

type CounterActions = INC | DEC;

export const counter: Reducer<number, CounterActions> = (state = 0, action) => {
  switch (action.type) {
    case "INC":
      return state + action.payload;
    case "DEC":
      return state - action.payload;
    default:
      return state;
  }
};

export const inc: ActionCreator<INC> = (payload: number) => ({
  type: "INC",
  payload
});

export const dec: ActionCreator<DEC> = (payload: number) => ({
  type: "DEC",
  payload
});
