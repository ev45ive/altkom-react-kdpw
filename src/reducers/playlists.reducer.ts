import { Reducer, Action, ActionCreator } from "redux";
import { Playlist } from "../models/Playlist";

type PlaylistsState = {
  items: Playlist[];
  selectedId: Playlist["id"] | null;
};

interface PLAYLISTS_LOAD extends Action<"PLAYLISTS_LOAD"> {
  payload: Playlist[];
}

interface PLAYLISTS_SELECT extends Action<"PLAYLISTS_SELECT"> {
  payload: Playlist["id"];
}

interface PLAYLISTS_UPDATE extends Action<"PLAYLISTS_UPDATE"> {
  payload: Playlist;
}

type Actions = PLAYLISTS_LOAD | PLAYLISTS_SELECT | PLAYLISTS_UPDATE;

export const playlists: Reducer<PlaylistsState, Actions> = (
  state = {
    items: [],
    selectedId: null
  },
  action
) => {
  switch (action.type) {
    case "PLAYLISTS_LOAD":
      return { ...state, items: action.payload };

    case "PLAYLISTS_SELECT":
      return { ...state, selectedId: action.payload };

    case "PLAYLISTS_UPDATE": {
      const draft = action.payload;
      return {
        ...state,
        items: state.items.map(p => (p.id == draft.id ? draft : p))
      };
    }
    default:
      return state;
  }
};

export const playlistsSelect: ActionCreator<PLAYLISTS_SELECT> = (
    playlist: Playlist
  ) => ({
    type: "PLAYLISTS_SELECT",
    payload: playlist.id
  });
export const playlistsSelectId: ActionCreator<PLAYLISTS_SELECT> = (
  payload: Playlist["id"]
) => ({
  type: "PLAYLISTS_SELECT",
  payload
});


export const playlistsUpdate: ActionCreator<PLAYLISTS_UPDATE> = (
    playlist: Playlist
  ) => ({
    type: "PLAYLISTS_UPDATE",
    payload: playlist
  });


export const selectedPlaylistSelector = (state: {
  playlists: PlaylistsState;
}) =>
  state.playlists.items.find(p => p.id === state.playlists.selectedId) || null;
