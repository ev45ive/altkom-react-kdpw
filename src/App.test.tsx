import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import renderer from "react-test-renderer/shallow";

it("renders without crashing", () => {
  const shallow = renderer.createRenderer()
  const component = shallow.render(<App />);
  
  expect(component).toBeTruthy()
});
