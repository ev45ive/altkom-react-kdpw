import { AuthService } from "./services/AuthService";
import { MusicSearchService } from "./services/MusicSearchService";
import React from "react";
import { Album } from "./models/Album";

export const auth = new AuthService(
  "https://accounts.spotify.com/authorize",
  "70599ee5812a4a16abd861625a38f5a6",
  "http://localhost:3000/"
);

export const musicSearch = new MusicSearchService(
  "https://api.spotify.com/v1/search",
  auth
);

export const musicContext = React.createContext<{
  query: string;
  results: Album[];
  search(query: string): void;
}>({
  query: "",
  results: [],
  search(query: string) {
    throw "No provider for MusicContext";
  }
});
