function echo(msg, err){

	return new Promise((resolve, reject)=>{
		setTimeout(()=> err? reject('Ups!') : resolve(msg), 2000)
    })
}


p = echo('HEllo',true)
	.then(resp => {
		return echo(resp + ' Alice!')
    })
// 	.catch(err => Promise.reject('Some error occured : '+err))
	.catch(err =>('Some error occured : '+err))

p.then(console.log)
p.then(console.log)