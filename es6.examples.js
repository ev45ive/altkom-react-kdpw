var person = { 
		name:'Alice', 
		company: { 
			name:'Acme', 
			address:{ street:'Sezamkowa', number: '12' }
        }, 
		scores:[1,324,345,36]
};

getInfo = ({
    name:userName, 
    age: userAge = ' age unknown ',
    company:{ name: companyName },
}) =>  ({
	info: `${userName}, ${userAge} - ${companyName}`,
	dateCreated: new Date()
});

getInfo(person)