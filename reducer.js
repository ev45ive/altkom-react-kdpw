inc = { type:'INC', payload:1 };
dec = { type:'DEC', payload:1 };
addTodo = name => ({ type:'ADD_TODO', payload:{ name }});

reducer = (state, action) => {
    switch(action.type) {
        case 'INC': return {...state, counter: state.counter + action.payload };
        case 'DEC': return {...state, counter: state.counter - action.payload };
        case 'ADD_TODO': return { ...state, todos:[ ...state.todos, action.payload ] }
        default: return state;
    }
};

[inc,inc,addTodo('placki'), dec,inc].reduce( reducer, {
	todos:[],
	counter:0
})